# Actions-OpenWrt-Lienol-K2P

> This repository is generated from P3TERX/Actions-OpenWrt

Use Github Actions to automatically compile Lienol's Modified OpenWrt source for K2P

## Usage

The build process will be triggered when:

- It's 8:00 on Friday
- A new push was made to the master branch
- A new release was published
- This repository was starred

When the build is complete, click the Artifacts button in the upper right corner of the Actions page to download the binaries.

## Acknowledgments

- [Microsoft](https://www.microsoft.com)
- [Microsoft Azure](https://azure.microsoft.com)
- [GitHub](https://github.com)
- [GitHub Actions](https://github.com/features/actions)
- [tmate](https://github.com/tmate-io/tmate)
- [mxschmitt/action-tmate](https://github.com/mxschmitt/action-tmate)
- [csexton/debugger-action](https://github.com/csexton/debugger-action)
- [Cisco](https://www.cisco.com/)
- [OpenWrt](https://github.com/openwrt/openwrt)
- [Lienol's Modified OpenWrt source](https://github.com/Lienol/openwrt)
- [Actions-OpenWrt](https://github.com/P3TERX/Actions-OpenWrt)

## License

[MIT & GPLv3](https://github.com/DreamWalkerXZ/Actions-OpenWrt-Lienol-K2P/blob/master/LICENSE)
